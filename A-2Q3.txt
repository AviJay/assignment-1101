#include <stdio.h>
int main() {
    char chr;
    int lowercase, uppercase;
    printf("Enter the character: ");
    scanf("%c", &chr);

    lowercase = (chr == 'a' || chr == 'e' || chr == 'i' || chr == 'o' || chr == 'u');
    uppercase = (chr == 'A' || chr == 'E' || chr == 'I' || chr == 'O' || chr == 'U');

    if (lowercase || uppercase)
        printf("%c is a vowel.", chr);
    else
        printf("%c is a consonant.", chr);
    return 0;
}
